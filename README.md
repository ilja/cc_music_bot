# A Creative music bot
## What?

This powers a bot that looks for music released under a [Creative Commons](https://creativecommons.org/) license and posts it to [Pleroma](https://pleroma.social).

Currently it looks for music on [Jamendo](https://www.jamendo.com/).

## Why?

Why not 🙃 I like [Free Software](https://en.wikipedia.org/wiki/Free_and_open-source_software) and I like [Free Culture](https://en.wikipedia.org/wiki/Free-culture_movement) in general. If rightsholders don't want me to listen and share their music, then maybe I shouldn't. However, if there are artists out there whos music I'm allowed to listen to and share, then I want to discover and promote them.

## How to set up the bot

1. Get the script locally
2. Run the `main.py` script once from the location you'll run it later. It will create a file called settings.json and has some content with placeholders
3. Fill in the placeholders in the newly created settings.json file
4. Now you can run it in regular intervals using something like e.g. cron

## Contributing

Feel free to file issues or make PR's if you see problems or have ideas on how to make this bot better. Currently it only uses Jamendo as a source, I'm not against having multiple sources, but please make sure the quality is somewhat ok and ofcourse that the licensing is compliant with the goal of this bot. There may also be some things hard-coded where it makes more sense to not have them hard-coded. Feel free to make PR's for that as well if that helps you.

The code is formatted using [Black](https://pypi.org/project/black/).

## License

    A bot to post Creative Commons released music to Pleroma.
    Copyright (C) 2022  Ilja

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
