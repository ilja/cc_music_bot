#!/bin/python3

import json
import os
import random
import re
import requests
import shutil

##
# Values
##

jamendo_base_url = "https://api.jamendo.com/v3.0/tracks/?format=json&limit=1&include=musicinfo&durationbetween=0_420"
temp_dir = os.path.abspath("") + "/tmp/"
settings_file = "settings.json"
default_settings = {
    "pleroma": {
        "instance": "<INSTANCE_DOMAIN> e.g. ilja.space",
        "username": "<USERNAME> e.g. cc_music_bot",
        "authorization": "<BEARER_TOKEN> e.g. Bearer t8te5MoRIIatJd43XhGjuvtWG9TSwCVPd3nCXZEQzP7e",
        "status_expires_in": 604800,
    },
    "jamendo": {
        "client_id": "<CLIENT_ID> Get this at https://developer.jamendo.com",
        "only_free_culture_music": "true",
        "result_size": 20,
    },
}
settings = None

message_template = """<a href='{SOURCE}'>{TITLE}</a> by {ARTIST}

from the album {ALBUM}

License: <a href='{LICENSE_URL}'>{LICENSE_NAME}</a>

#CreativeCommons {GENRES}"""

##
# Debug helper
# See <https://stackoverflow.com/questions/60456113/print-argument-values-to-functions-in-stack-trace>
##
def print_stack_arguments(func):
    def new_func(*original_args, **original_kwargs):
        try:
            return func(*original_args, **original_kwargs)
        except Exception as e:
            print("Function: ", func.__name__)
            print("Args: ", original_args)
            print("Kwargs: ", original_kwargs)
            print(e)
            raise

    return new_func


##
# Functions
##


@print_stack_arguments
def get_settings_from_file(settings_file, default_settings_content):
    try:
        with open(settings_file, "r") as f:
            settings = json.load(f)
    except:
        f = open(settings_file, "w")
        settings = default_settings_content
        json.dump(settings, f, indent=4)
    if settings == default_settings_content:
        print(
            "Please change the placeholders in the settings file "
            + os.getcwd()
            + "/"
            + settings_file
        )
        exit()
    return settings


@print_stack_arguments
def build_url(jamendo_settings):
    return (
        jamendo_base_url
        + get_boost_params()
        + get_fuzzytags_params()
        + get_gender_params()
        + get_client_id_params(jamendo_settings)
        + get_offset_params(jamendo_settings)
        + get_free_culture_params(jamendo_settings)
    )


@print_stack_arguments
def get_free_culture_params(jamendo_settings):
    if jamendo_settings["only_free_culture_music"] == "true":
        return "&ccnd=false&ccnc=false"
    else:
        return random.choice(
            ["", "&ccnd=false", "&ccnc=false", "&ccnd=false&ccnc=false"]
        )


@print_stack_arguments
def get_boost_params():
    return "&boost=" + random.choice(
        [
            "buzzrate",
            "downloads_week",
            "downloads_month",
            "downloads_total",
            "listens_week",
            "listens_month",
            "listens_total",
            "popularity_week",
            "popularity_month",
            "popularity_total",
        ]
    )


@print_stack_arguments
def get_fuzzytags_params():
    return "&fuzzytags=" + random.choice(
        [
            "metal",
            "rock",
            "jazz",
            "blues",
            "folk",
            "metal+rock",
            "metal+folk",
            "rock+blues",
        ]
    )


@print_stack_arguments
def get_gender_params():
    return random.choice(["&gender=female", "&gender=male", ""])


@print_stack_arguments
def get_offset_params(jamendo_settings):
    return "&offset=" + str(random.randint(0, jamendo_settings["result_size"]))


@print_stack_arguments
def get_client_id_params(jamendo_settings):
    return "&client_id=" + jamendo_settings["client_id"]


@print_stack_arguments
def build_genre_list(genres):
    genre_list = ""
    for genre in genres:
        genre_list += "#" + genre + " "
    return genre_list


@print_stack_arguments
def build_license_name(license_url):
    reg = re.compile(
        "https?://creativecommons.org/licenses/(by[a-z,\-]*)/([0-9,\.]*).*"
    )
    license, version = re.findall(reg, license_url)[0]
    return "CC " + license.upper() + " " + version


@print_stack_arguments
def build_message(song):
    return (
        message_template.replace("{TITLE}", song["name"])
        .replace("{ARTIST}", song["artist_name"])
        .replace("{ALBUM}", song["album_name"])
        .replace("{LICENSE_URL}", song["license_ccurl"])
        .replace("{LICENSE_NAME}", build_license_name(song["license_ccurl"]))
        .replace("{SOURCE}", song["shareurl"])
        .replace("{GENRES}", build_genre_list(song["musicinfo"]["tags"]["genres"]))
        .replace("\n", "<br>")
    )


@print_stack_arguments
def post_to_pleroma(status, media_id_audio, media_id_image, pleroma_settings):
    post_url = "https://{INSTANCE}/api/v1/statuses".replace(
        "{INSTANCE}", pleroma_settings["instance"]
    )
    headers = {
        "Authorization": pleroma_settings["authorization"],
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json",
    }
    # 604800 is 60*60*24*7 and thus about a week that statuses are kept. Then they are deleted.
    data = (
        '{"content_type": "text/html", "expires_in": {EXPIRES_IN}, "status": "{STATUS}", "media_ids": ["{MEDIA_ID_AUDIO}", "{MEDIA_ID_IMAGE}"]}'.replace(
            "{EXPIRES_IN}", str(pleroma_settings["status_expires_in"])
        )
        .replace("{MEDIA_ID_AUDIO}", media_id_audio)
        .replace("{MEDIA_ID_IMAGE}", media_id_image)
        .replace("{STATUS}", status)
    )

    requests.post(
        post_url, headers=headers, data=data.encode("utf-8")
    ).raise_for_status()


@print_stack_arguments
def upload_media(location, file_name, description, pleroma_settings):
    post_url = "https://{INSTANCE}/api/v1/media".replace(
        "{INSTANCE}", pleroma_settings["instance"]
    )
    files = {"file": (file_name, open(location, "rb"))}
    headers = {"Authorization": pleroma_settings["authorization"]}
    response = requests.post(
        post_url,
        headers=headers,
        data={"upload_type": "standard", "upload_to": "0"},
        files=files,
    )

    response.raise_for_status()

    media_id = response.json()["id"]
    put_url = "https://{INSTANCE}/api/v1/media/{MEDIA_ID}".replace(
        "{INSTANCE}", pleroma_settings["instance"]
    ).replace("{MEDIA_ID}", media_id)
    headers = {"Authorization": pleroma_settings["authorization"]}
    requests.put(
        put_url, headers=headers, data={"description": description}
    ).raise_for_status()

    return media_id


@print_stack_arguments
def download_file(url, filename):
    absolute_file = temp_dir + filename.replace("/", "_")
    if not os.path.exists(temp_dir):
        os.mkdir(temp_dir)
    myfile = requests.get(url)
    open(absolute_file, "wb").write(myfile.content)
    return absolute_file


@print_stack_arguments
def exit_if_it_has_known_problems_i_cant_work_around(song):
    # I can't filter on audiodownload_allowed :(
    # I checked the API docs and also tried it
    # <https://developer.jamendo.com/v3.0/tracks>
    if not song["audiodownload_allowed"] or song["license_ccurl"] == "":
        exit(1)


##
# Logic
##

settings = get_settings_from_file(settings_file, default_settings)

response = requests.get(build_url(settings["jamendo"]))

if response.status_code == 200 and len(response.json()["results"]) > 0:
    song = response.json()["results"][0]
    pleroma_settings = settings["pleroma"]

    exit_if_it_has_known_problems_i_cant_work_around(song)

    name_audio_file = song["artist_name"] + " - " + song["name"] + ".mp3"
    name_image_file = song["album_name"] + ".jpeg"

    location_audio = download_file(song["audiodownload"], name_audio_file)
    location_image = download_file(song["album_image"], name_image_file)

    description_audio = song["artist_name"] + " - " + song["name"]
    description_image = (
        "Album cover from the album "
        + song["album_name"]
        + " by "
        + song["artist_name"]
    )

    media_id_audio = upload_media(
        location_audio, name_audio_file, description_audio, pleroma_settings
    )
    media_id_image = upload_media(
        location_image, name_image_file, description_image, pleroma_settings
    )

    post_to_pleroma(
        build_message(song), media_id_audio, media_id_image, pleroma_settings
    )
    shutil.rmtree(temp_dir)
else:
    print(response.content)
    exit(1)
